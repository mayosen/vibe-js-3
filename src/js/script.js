"use strict";

const personalMovieDB = {
  count: 0,
  movies: {},
  actors: {},
  genres: [],
  privat: false
};

function showMyDB() {
  if (!personalMovieDB.privat) {
    console.log(personalMovieDB);
  }
}

function writeYourGenres() {
  for (let i = 0; i < 3; i++) {
    let genre = prompt(`Ваш любимый жанр под номером ${i + 1}`);
    personalMovieDB.genres.push(genre);
  }
}

writeYourGenres();
showMyDB();
